<?php

namespace Drupal\graphql_search_api_solr\Query;

use Drupal\Core\Logger\LoggerChannel;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Pager\Pager;
use Drupal\Core\Pager\PagerManager;
use Drupal\search_api\Entity\Index;
use Drupal\search_api\Query\ConditionGroup;
use Drupal\search_api\Query\QueryInterface;
use Drupal\search_api\ParseMode\ParseModeInterface;
use Drupal\search_api\Query\ConditionGroupInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Wrapper around \Drupal\search_api\Query\Query.
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.StaticAccess)
 */
class SearchApiQuery {

  use StringTranslationTrait;

  const SPECIAL_CHARS = [
    // \ must be the first replacement to avoid unnecessarily \\.
    '\\',
    '+',
    '-',
    '&',
    '|',
    '!',
    '(',
    ')',
    '{',
    '}',
    '[',
    ']',
    '~',
    '*',
    '?',
    ':',
    '/',
  ];

  /**
   * Le parse mode (direct, edismax ...).
   */
  private ParseModeInterface $parseMode;

  /**
   * Solr search index.
   */
  private ?Index $index = NULL;

  /**
   * Request.
   */
  private QueryInterface $query;

  /**
   * Loaded results.
   */
  private mixed $resultEntities;

  /**
   * Service logger.
   */
  private LoggerChannelInterface $logger;

  /**
   * Service pager.manager.
   */
  private PagerManager $pagerManager;

  /**
   * Pagination.
   */
  private ?Pager $pager = NULL;

  /**
   * Number of pages, 0 <=> no limite.
   *
   * @var int
   */
  protected $limit = 0;

  /**
   * Constructor.
   *
   * @param string $index
   *   Machine name.
   * @param \Drupal\search_api\ParseMode\ParseModeInterface $parseMode
   *   Parse mode (direct, edismax ...).
   * @param \Drupal\Core\Logger\LoggerChannel $logger
   *   Service logger.
   * @param \Drupal\Core\Pager\PagerManager $pagerManager
   *   Service pager.manager.
   *
   * @throws \Drupal\search_api\SearchApiException
   *
   * @see \Drupal\epppd_search\Query\SearchApiQueryFactory
   */
  public function __construct(
    string $index,
    ParseModeInterface $parseMode,
    LoggerChannel $logger,
    PagerManager $pagerManager,
  ) {
    $this->parseMode = $parseMode;
    $this->index = Index::load($index);
    $this->query = $this->index->query();
    $this->query->setParseMode($this->parseMode);
    $this->logger = $logger;
    $this->pagerManager = $pagerManager;
  }

  /**
   * Getter.
   *
   * @return \Drupal\search_api\Entity\Index
   *   Search API Solr index.
   */
  public function getIndex() {
    return $this->index;
  }

  /**
   * Ping for availability.
   *
   * @return bool
   *   Weither or not the server is up and running.
   */
  public function serverIsAvailable(): bool {
    $server = $this->index->getServerInstance();
    return $server->isAvailable();
  }

  /**
   * Facets setting formatted for search_api.
   *
   * @param mixed[] $facetsFilters
   *   Array given to SearchApiSolrBackend::setFacets.
   *
   * @return self
   *   Instance of \Drupal\epppd_search\Query\SearchApiQuery.
   *
   * @see \Drupal\search_api_solr\Plugin\search_api\backend\SearchApiSolrBackend::setFacets
   */
  public function setFacets(array $facetsFilters): self {
    $this->query->setOption('search_api_facets', $facetsFilters);
    return $this;
  }

  /**
   * Returns $this query object with method chaining capabilities.
   *
   * @return self
   *   Instance of \Drupal\epppd_search\Query\SearchApiQuery.
   */
  public function getQuery(): self {
    return $this;
  }

  /**
   * Fulltext search.
   *
   * @param mixed $keywords
   *   Searched text.
   *
   * @return self
   *   Methods chaining.
   */
  public function setKeys($keywords): self {
    $keywords = urldecode($keywords);
    foreach (self::SPECIAL_CHARS as $characterToEscape) {
      $keywords = str_replace($characterToEscape, "\\" . $characterToEscape, $keywords);
    }
    $this->query->keys($keywords);
    return $this;
  }

  /**
   * Paginated query.
   *
   * @param string $field
   *   Indexed field in search_api.
   * @param mixed $value
   *   Filter value.
   * @param string $operator
   *   Filter operator.
   *
   * @return self
   *   Methods chaining.
   */
  public function addCondition(string $field, $value, $operator = NULL): self {
    $this->query->addCondition($field, $value, $operator);
    return $this;
  }

  /**
   * Tri.
   *
   * @param string $field
   *   Field from search_api.
   * @param string $direction
   *   DESC or ASC.
   *
   * @return self
   *   Methods chaining.
   */
  public function addSort($field, $direction): self {
    $this->query->sort($field, $direction);
    return $this;
  }

  /**
   * Condition group.
   *
   * @param string $conjunction
   *   Conjunction ...
   * @param string[] $tags
   *   Debug / hook tags.
   *
   * @return \Drupal\search_api\Query\ConditionGroup
   *   Grouped condition
   */
  public function createConditionGroup($conjunction = 'AND', array $tags = []) {
    return new ConditionGroup($conjunction, $tags);
  }

  /**
   * Adds condition group to query.
   *
   * @param \Drupal\search_api\Query\ConditionGroupInterface $condition_group
   *   Conditions.
   *
   * @return self
   *   Methods chaining.
   */
  public function addConditionGroup(ConditionGroupInterface $condition_group) {
    $this->query->addConditionGroup($condition_group);
    return $this;
  }

  /**
   * Pager settings.
   *
   * @param int $limit
   *   Nb items per page, 0 <=> no pager.
   *
   * @return self
   *   Methods chaining.
   *
   * @see ::getResultsItems()
   */
  public function setLimit(int $limit): self {
    $this->limit = $limit;
    return $this;
  }

  /**
   * Results interval.
   *
   * @param int $offset
   *   Start of results.
   * @param int $limit
   *   Nb of requested items.
   *
   * @return self
   *   Methods chaining.
   */
  public function setRange(int $offset, int $limit): self {
    $this->query->range($offset, $limit);
    return $this;
  }

  /**
   * Retrieve results set.
   *
   * @return mixed
   *   Gives extra data and facets results.
   *
   * @see www/modules/contrib/search_api/src/Query/Query::preExecute
   *   A query is executed only once.
   */
  public function getResultsSet() {

    if (!$this->serverIsAvailable()) {
      $this->logger->error($this->t('Search server is unavailable'));
      return NULL;
    }

    $resultSet = $this->query->execute();

    if ($this->limit && empty($this->pager)) {
      $this->pager = $this->pagerManager->createPager($resultSet->getResultCount(), $this->limit);
    }

    return $resultSet;
  }

  /**
   * Results count helper.
   *
   * @return int
   *   Number of results for the current query.
   */
  public function countResults(): int {
    return $this->query->execute()->getResultCount();
  }

  /**
   * Search results retrieval.
   *
   * @return mixed[]
   *   Items from search_api.
   */
  public function getResultsItems(): array {

    return $this->getResultsSet()->getResultItems();
  }

  /**
   * Search results retrieval.
   *
   * @return mixed[]
   *   Entités Drupal.
   */
  public function getResultsEntities(): array {
    if (empty($this->resultEntities)) {

      $resultsItems = $this->getResultsItems();

      $this->resultEntities = array_map(function (mixed $item) {
        $originalObject = $item->getOriginalObject();
        /* @phpstan-ignore-next-line */
        return $originalObject->getEntity();
      }, $resultsItems);
    }
    return $this->resultEntities;
  }

  /**
   * Wrapper.
   *
   * @param string $tag
   *   SearchQuery tag.
   *
   * @return self
   *   Methods chaining.
   */
  public function addTag($tag) {
    $this->query->addTag($tag);
    return $this;
  }

}
