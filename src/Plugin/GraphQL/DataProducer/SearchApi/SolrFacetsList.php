<?php

namespace Drupal\graphql_search_api_solr\Plugin\GraphQL\DataProducer\SearchApi;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\graphql_search_api_solr\Plugin\GraphQL\Wrappers\ResultSet;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\graphql_search_api_solr\Plugin\GraphQL\Wrappers\FacetsList;

/**
 * Solr Search results count data producer.
 *
 * @DataProducer(
 *   id = "solr_facets_list",
 *   name = @Translation("Solr facets"),
 *   description = @Translation("Display active facets data."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Solr facets")
 *   ),
 *   consumes = {
 *     "result_set" = @ContextDefinition("any",
 *       label = @Translation("Facets"),
 *       required = FALSE,
 *     ),
 *   }
 * )
 */
class SolrFacetsList extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Mapping between search_api and Solr field names.
   *
   * @var array
   */
  protected $fieldNames;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * Executes the Solr search.
   *
   * @param \Drupal\graphql_search_api_solr\Plugin\GraphQL\Wrappers\ResultSet $resultSet
   *   Results set.
   *
   * @return mixed[]
   *   Array FacetsList
   */
  public function resolve(ResultSet $resultSet) {

    $facets = [];
    $results = $resultSet->getResults();
    $langcode = $resultSet->getLangcode();
    $facetSet = $results->getExtraData('facet_set')->getFacets();
    $index = $results->getQuery()->getIndex();
    // @phpstan-ignore-next-line
    $this->fieldNames = $index->getServerInstance()
      ->getBackend()->getSolrFieldNames($index);

    foreach ($facetSet as $facetName => $facet) {
      $facets[] = $this->getFacet($facetName, $facet, $langcode);
    }

    return $facets;
  }

  /**
   * This function can be overrided in a child class.
   *
   * @param string $facetName
   *   Name.
   * @param \Solarium\Component\Result\Facet\Field $facetValues
   *   Values.
   * @param string $langcode
   *   Language.
   *
   * @return \Drupal\graphql_search_api_solr\Plugin\GraphQL\Wrappers\FacetsList
   *   Facet name and values.
   */
  protected function getFacet($facetName, $facetValues, $langcode) {
    $searchApiFacetName = array_search($facetName, $this->fieldNames);
    return new FacetsList($searchApiFacetName, $facetValues, $langcode);
  }

}
