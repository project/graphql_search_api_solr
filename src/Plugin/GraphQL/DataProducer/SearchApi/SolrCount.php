<?php

namespace Drupal\graphql_search_api_solr\Plugin\GraphQL\DataProducer\SearchApi;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\graphql_search_api_solr\Plugin\GraphQL\Wrappers\ResultSet;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;

/**
 * Solr Search results count data producer.
 *
 * @DataProducer(
 *   id = "solr_count",
 *   name = @Translation("Solr search"),
 *   description = @Translation("Executes a Solr search."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Solr result")
 *   ),
 *   consumes = {
 *     "result_set" = @ContextDefinition("any",
 *       label = @Translation("Page offset"),
 *       required = FALSE,
 *     ),
 *   }
 * )
 */
class SolrCount extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * Executes the Solr search.
   *
   * @param \Drupal\graphql_search_api_solr\Plugin\GraphQL\Wrappers\ResultSet $resultSet
   *   Results set.
   *
   * @return int
   *   Number of results.
   */
  public function resolve(ResultSet $resultSet) {
    return $resultSet->getResults()->getResultCount();
  }

}
