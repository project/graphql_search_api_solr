<?php

namespace Drupal\graphql_search_api_solr\Plugin\GraphQL\DataProducer\SearchApi;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\graphql_search_api_solr\Plugin\GraphQL\Wrappers\ResultSet;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;

/**
 * Solr Search results count data producer.
 *
 * @DataProducer(
 *   id = "solr_documents",
 *   name = @Translation("Solr documents"),
 *   description = @Translation("Display results documents."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Solr documents")
 *   ),
 *   consumes = {
 *     "result_set" = @ContextDefinition("any",
 *       label = @Translation("Facets"),
 *       required = TRUE,
 *     ),
 *     "document_class" = @ContextDefinition("string",
 *       label = @Translation("Documents class"),
 *       required = TRUE,
 *     ),
 *   }
 * )
 */
class SolrDocuments extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * Executes the Solr search.
   *
   * @param \Drupal\graphql_search_api_solr\Plugin\GraphQL\Wrappers\ResultSet $resultSet
   *   Results set containing results items.
   * @param string $documentClass
   *   Class used to create results objects from search_api results items.
   *
   * @return mixed[]
   *   Array of graphql objects.
   */
  public function resolve(ResultSet $resultSet, $documentClass) {

    $results = [];
    $solrResponse = $resultSet->getResults()->getExtraData('search_api_solr_response')['response'];

    if ($resultSet->getResults()->getResultCount() < $solrResponse['start']) {
      return [];
    }

    $items = $resultSet->getResults()->getResultItems();

    foreach ($items as $item) {
      // @phpstan-ignore-next-line
      $results[] = new $documentClass($item);
    }

    return $results;
  }

}
