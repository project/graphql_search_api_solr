<?php

namespace Drupal\graphql_search_api_solr\Plugin\GraphQL\DataProducer\SearchApi;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql_search_api_solr\Query\SearchApiQuery;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\graphql_search_api_solr\Plugin\GraphQL\Wrappers\ResultSet;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;

/**
 * Solr Search data producer.
 *
 * @DataProducer(
 *   id = "solr_search",
 *   name = @Translation("Solr search"),
 *   description = @Translation("Executes a Solr search."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Solr result")
 *   ),
 *   consumes = {
 *     "index" = @ContextDefinition("string",
 *       label = @Translation("Solr Index"),
 *       required = TRUE,
 *     ),
 *     "offset" = @ContextDefinition("integer",
 *       label = @Translation("Page offset"),
 *       required = FALSE,
 *     ),
 *     "limit" = @ContextDefinition("integer",
 *       label = @Translation("Page limit"),
 *       required = FALSE,
 *     ),
 *     "query" = @ContextDefinition("string",
 *       label = @Translation("Query string"),
 *       required = FALSE,
 *     ),
 *     "facets" = @ContextDefinition("any",
 *       label = @Translation("Facets"),
 *       required = FALSE,
 *     ),
 *     "filters" = @ContextDefinition("any",
 *       label = @Translation("Filters"),
 *       required = FALSE,
 *     ),
 *     "langcode" = @ContextDefinition("any",
 *       label = @Translation("Langcode"),
 *       required = FALSE,
 *     ),
 *     "sort" = @ContextDefinition("any",
 *       label = @Translation("Sort"),
 *       required = FALSE,
 *     ),
 *   }
 * )
 */
class SolrSearch extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Paramétrage des facettes.
   */
  protected const DEFAULT_FACETS_DATA = [
    'min_count' => -1,
    'operator' => 'and',
    'missing' => FALSE,
    'limit' => 0,
  ];

  /**
   * Parse mode manager service.
   *
   * @var \Drupal\search_api\ParseMode\ParseModePluginManager
   */
  protected $parseModeManager;

  /**
   * The module logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Pager manager.
   *
   * @var \Drupal\Core\Pager\PagerManager
   */
  protected $pagerManager;

  /**
   * SolrSearch constructor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\search_api\ParseMode\ParseModePluginManager $parseModeManager
   *   Parse mode manager service.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $factory
   *   Service logger.
   * @param \Drupal\Core\Pager\PagerManager $pagerManager
   *   Pager manager service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    $parseModeManager,
    LoggerChannelFactoryInterface $logger_factory,
    $pagerManager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->parseModeManager = $parseModeManager;
    $this->logger = $logger_factory->get("graphql_search_api_solr");
    $this->pagerManager = $pagerManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.search_api.parse_mode'),
      $container->get('logger.factory'),
      $container->get('pager.manager')
    );
  }

  /**
   * Executes the Solr search.
   *
   * @param string $index
   *   Search api solr index name.
   * @param int $offset
   *   Start of pagination.
   * @param int $limit
   *   Number of results.
   * @param string $query
   *   Query string.
   * @param mixed[] $facets
   *   Facet parameters.
   * @param mixed[] $filters
   *   Fields conditions.
   * @param string $langcode
   *   Langcodes filter.
   * @param mixed[] $sort
   *   Sort order.
   *
   * @return \Drupal\search_api\Query\ResultSet|null
   *   Results set.
   */
  public function resolve($index, $offset, $limit, $query, $facets, $filters, $langcode, $sort) {

    $parseMode = $this->parseModeManager->createInstance('direct');
    $searchApiQuery = new SearchApiQuery(
      $index,
      $parseMode,
      $this->logger,
      $this->pagerManager
    );

    $searchApiQuery->setRange($offset, $limit);

    $index = $searchApiQuery->getIndex();
    // @phpstan-ignore-next-line
    $fieldNames = $index->getServerInstance()
      ->getBackend()->getSolrFieldNames($index);
    $searchApiQuery->setFacets($this->getFacetsData($facets, $fieldNames));

    if ($query) {
      $searchApiQuery->setKeys($query);
    }

    $this->setFacetsConditionGroup($searchApiQuery, $facets);
    $this->setFiltersConditionGroup($searchApiQuery, $filters, $langcode);

    if ($sort) {
      $searchApiQuery->addSort($sort['field'], $sort['order']);
    }

    return new ResultSet(
      $searchApiQuery->getResultsSet(),
      $langcode
    );
  }

  /**
   * Provides facet configuration overriding self::FACETS_DATA.
   *
   * @param mixed[]|null $facets
   *   Array of facet configuration overrides.
   * @param mixed[] $fieldNames
   *   Array of search_api fields config.
   *
   * @return mixed[]
   *   New facets configuration.
   */
  public function getFacetsData($facets, $fieldNames) {

    $facetsData = [];
    $facets = is_array($facets) ? $facets : [];

    foreach ($facets as $facetData) {

      $facetData += static::DEFAULT_FACETS_DATA;

      if (isset($facetData['values'])) {
        unset($facetData['values']);
      }

      if (empty($fieldNames[$facetData['field']])) {
        continue;
      }

      $facetField = $fieldNames[$facetData['field']];
      $facetsData[$facetField] = $facetData;
    }

    return $facetsData;
  }

  /**
   * Facets query wrapper.
   *
   * @param \Drupal\graphql_search_api_solr\Query\SearchApiQuery $searchQuery
   *   Search query object.
   * @param mixed[]|null $facets
   *   Facets filters.
   */
  protected function setFacetsConditionGroup(&$searchQuery, $facets): void {
    if (empty($facets)) {
      return;
    }

    foreach ($facets as $facet) {
      if (!isset($facet['values'])) {
        continue;
      }

      $operator = $facet['operator'] ?? 'or';
      $conditionsGroup = $searchQuery->createConditionGroup($operator, ['graphql_facets']);
      foreach ($facet['values'] as $value) {
        $conditionsGroup->addCondition($facet['field'], $value);
      }
      $searchQuery->addConditionGroup($conditionsGroup);
    }
  }

  /**
   * Filters query wrapper.
   *
   * @param \Drupal\graphql_search_api_solr\Query\SearchApiQuery $searchQuery
   *   Search query object.
   * @param mixed[]|null $filters
   *   Query condition filters.
   * @param string|null $langcode
   *   Langcode for translations.
   *
   * @return void
   *   Updates $searchQuery by reference, always returns NULL.
   */
  protected function setFiltersConditionGroup($searchQuery, $filters, $langcode = NULL) {

    if (empty($filters)) {
      return NULL;
    }

    if (!empty($langcode)) {

      $langcodesConditionsGroup = $searchQuery->createConditionGroup(
        'OR',
        ['langcodes']
      );
      $langcodesConditionsGroup->addCondition('search_api_language', $langcode);

      $searchQuery->addConditionGroup($langcodesConditionsGroup);
    }

    foreach ($filters as $filtersGroup) {

      if (!isset($filtersGroup['conditions'])) {
        return NULL;
      }
      if (!isset($filtersGroup['conjunction'])) {
        $filtersGroup['conjunction'] = 'AND';
      }

      if ($filtersGroup['tag'] === 'langcodes') {

        foreach ($filtersGroup['conditions'] as $filter) {

          if ($filtersGroup['tag'] === 'langcodes') {
            $langcodesConditionsGroup->addCondition(
              $filter['field'],
              $filter['values'],
              $filter['operator']
            );
          }
        }

        continue;
      }

      $tag = $filtersGroup['tag'] ?? 'graphql_search_api_conditions';
      $conditionsGroup = $searchQuery->createConditionGroup(
        $filtersGroup['conjunction'],
        [$tag]
      );

      foreach ($filtersGroup['conditions'] as $filter) {

        if (!isset($filter['values'])) {
          continue;
        }

        if (empty($filter['operator'])) {
          $filter['operator'] = '=';
        }

        if (in_array(
          $filter['operator'],
          ['IN', 'NOT IN', 'BETWEEN', 'NOT BETWEEN'])
        ) {
          $conditionsGroup->addCondition($filter['field'], $filter['values'], $filter['operator']);
        }
        else {
          foreach ($filter['values'] as $value) {
            $filterOperator = $filter['operator'] ?? '=';
            $conditionsGroup->addCondition($filter['field'], $value, $filterOperator);
          }
        }
      }

      $searchQuery->addConditionGroup($conditionsGroup);
    }
  }

}
