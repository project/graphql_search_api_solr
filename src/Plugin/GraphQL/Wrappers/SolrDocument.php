<?php

namespace Drupal\graphql_search_api_solr\Plugin\GraphQL\Wrappers;

use Drupal\search_api\Item\Item;

/**
 * Class document wrapper.
 */
class SolrDocument {

  /**
   * Solr document ID.
   */
  public string $id;

  /**
   * Document node title.
   */
  public string $title;

  /**
   * Format du document.
   *
   * @var string
   */
  public string $format;

  /**
   * Thumbnail.
   *
   * @var string
   */
  public string $thumbnail;

  /**
   * Constructs data producer wrapper.
   *
   * @param \Drupal\search_api\Item\Item $item
   *   Indexed document data.
   */
  public function __construct(Item $item) {

    $this->id = preg_filter('/[^\d+]/', '', $item->getId());

    // Extend this class to set your own fields from $resultFields data.
    $resultFields = $item->getFields();
    // ...
  }

}
