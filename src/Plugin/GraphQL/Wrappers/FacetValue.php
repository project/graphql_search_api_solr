<?php

namespace Drupal\graphql_search_api_solr\Plugin\GraphQL\Wrappers;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class Facet wrapper.
 */
class FacetValue {

  use StringTranslationTrait;

  /**
   * Facet field.
   */
  public string $field;

  /**
   * Facet value.
   */
  public string $value;

  /**
   * Facet count.
   */
  public int $count;

  /**
   * Constructs data producer wrapper.
   *
   * @param string $field
   *   Facet field name.
   * @param string $value
   *   Facet value.
   * @param int $count
   *   Facet count.
   */
  public function __construct($field, $value, $count) {
    $this->field = $field;
    $this->value = $value;
    $this->count = $count;
  }

}
