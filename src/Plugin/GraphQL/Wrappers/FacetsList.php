<?php

namespace Drupal\graphql_search_api_solr\Plugin\GraphQL\Wrappers;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Solarium\Component\Result\Facet\Field;

/**
 * Class list of Facets wrapper.
 */
class FacetsList {

  use StringTranslationTrait;

  /**
   * Facet field name.
   */
  public string $field;

  /**
   * Facet values.
   */
  public mixed $values;

  /**
   * Display value langcode.
   */
  protected string $langcode;

  /**
   * Constructs data producer wrapper.
   *
   * @param string $facetName
   *   Facet name as defined in solr configuration.
   * @param \Solarium\Component\Result\Facet\Field $values
   *   Facet values.
   * @param string|null $langcode
   *   Value langcodes.
   */
  public function __construct($facetName, Field $values, $langcode = NULL) {

    $this->langcode = $langcode;
    $this->field = $facetName;
    foreach ($values as $value => $count) {
      if ($langcode) {
        $value = $this->t(strval($value), [], [
          'langcode' => $langcode,
        ])->__toString();
      }
      $this->values[] = new FacetValue($facetName, $value, $count);
    }
  }

  /**
   * Getter.
   *
   * @return mixed
   *   This values.
   */
  public function getValues() {
    return $this->values ?? [];
  }

}
