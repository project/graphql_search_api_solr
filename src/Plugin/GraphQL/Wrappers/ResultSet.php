<?php

namespace Drupal\graphql_search_api_solr\Plugin\GraphQL\Wrappers;

use Drupal\search_api\Query\ResultSet as BaseResultSet;

/**
 * Wrapper around result set adding langcode.
 */
class ResultSet {

  /**
   * ResultSet from search_api.
   *
   * @var \Drupal\search_api\Query\ResultSet
   */
  protected BaseResultSet $results;

  /**
   * Langcode from query.
   *
   * @var string|null
   */
  protected ?string $langcode;

  /**
   * Constructor.
   *
   * @param mixed $results
   *   Property value.
   * @param string|null $langcode
   *   Property value.
   */
  public function __construct(BaseResultSet $results, $langcode = NULL) {
    $this->results = $results;
    $this->langcode = $langcode;
  }

  /**
   * Getter.
   *
   * @return string
   *   Langcode.
   */
  public function getLangcode() {
    return $this->langcode;
  }

  /**
   * Getter.
   *
   * @return \Drupal\search_api\Query\ResultSet
   *   Result set.
   */
  public function getResults() {
    return $this->results;
  }

}
