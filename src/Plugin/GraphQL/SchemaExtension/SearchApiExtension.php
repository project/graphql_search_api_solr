<?php

namespace Drupal\graphql_search_api_solr\Plugin\GraphQL\SchemaExtension;

use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\Plugin\GraphQL\SchemaExtension\SdlSchemaExtensionPluginBase;

/**
 * Provides a base GraphQL schema search_api extension.
 */
abstract class SearchApiExtension extends SdlSchemaExtensionPluginBase {

  /**
   * Get the class used to set result properties.
   *
   * @return string
   *   Class reflecting index fields to be included in results.
   */
  abstract protected function getDocumentClass();

  /**
   * {@inheritdoc}
   */
  public function registerResolvers(ResolverRegistryInterface $registry): void {

    $builder = new ResolverBuilder();

    // Connect the solrSearch field to the solr_search data producer.
    $registry->addFieldResolver('Query', 'solrSearch',
      $builder->produce('solr_search')
        ->map('index', $builder->fromArgument('index'))
        ->map('offset', $builder->fromArgument('offset'))
        ->map('limit', $builder->fromArgument('limit'))
        ->map('query', $builder->fromArgument('query'))
        ->map('facets', $builder->fromArgument('facets'))
        ->map('filters', $builder->fromArgument('filters'))
        ->map('langcode', $builder->fromArgument('langcode'))
        ->map('sort', $builder->fromArgument('sort'))
    );

    $registry->addFieldResolver('SolrResult', 'count',
      $builder->produce('solr_count')
        ->map('result_set', $builder->fromParent())
    );

    $registry->addFieldResolver('SolrResult', 'facets',
      $builder->produce('solr_facets_list')
        ->map('result_set', $builder->fromParent())
    );

    $registry->addFieldResolver('SolrResult', 'documents',
      $builder->produce('solr_documents')
        ->map('result_set', $builder->fromParent())
        ->map('document_class', $builder->fromValue($this->getDocumentClass()))
    );
  }

}
