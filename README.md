GraphQL Search API Solr

INTRODUCTION
------------

This module provides a set of base classes to make solr search query with graphql 4.

REQUIREMENTS
------------

This module requires the following module:

 * Graphql - https://www.drupal.org/project/graphql
 * Search API Solr - https://www.drupal.org/project/search_api_solr


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

 1. Create your own schema definition and extend the following class:     
    \Drupal\graphql_search_api_solr\Plugin\GraphQL\SchemaExtension\SearchApiExtension.
    It needs an annotation such as:

      @SchemaExtension(
        id = "mediation_search_api_extension",
        name = "Mediation GraphQL Search API extension",
        description = "An extension that adds search_api_solr related fields.",
        schema = "{your_schema}"
      )

 2. Create your own SolrDocument class (you can extend SolrDocument),
    its path needs to be returned by the getDocumentClass method inherited
    by the Schema class from the first step.

 3. Copy this module directory graphql/ graphqls file to your schema definition
    and rename them according to your schema Class name. For the above, it would be:

    * {your_schema}_search_api_extension.base.graphqls
    * {your_schema}_search_api_extension.extension.graphqls

 4. Add your fields to the extension.graphqls file.

    Exemple:

      extend type SolrDocument {
        title
        format
        thumbnail
      }

Query example.
----------------

  query {
    solrSearch(
      index: "custom_search_api_page",
      offset: 0,
      limit: 10,
      query: "Pizza",
      facets: [
        {field: "formats", values:"video", min_count: 1}
      ],
      sort: {
        field: "created",
        order: "DESC"
      },
      langcode: "en",
      filters: [{
        conditions: [{
          field: "missing_languages", values: ["fr"], operator: "IN",
	  # Add your own field grouped conditions
	}],
	conjunction: "OR",
        tag: "langcodes"
      }],
    ) {
      count,
      documents {
        id,
        title,
        # Add your own document fields:
        format,
        thumbnail
      },
      facets {
        field,
        values {
          field
          value
          count
        }
      }
    }
  }
